import kivy
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.widget import Widget
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import ScreenManager,  Screen, FadeTransition
import models.users
import models.productos
from kivy.uix.textinput import TextInput
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label 
user_type = ""
#openedPop = 0
# login = Builder.load_file('login.kv')
# Popup


sm = ScreenManager()




class P_error_passwords_not_equals(FloatLayout):
    pass
def show_popup_error_reg_passwords_not():
    show = P_error_passwords_not_equals()
    global popupwindow_err_register_passwords
    popupwindow_err_register_passwords = Popup(title="Atention",
                        content=show, size_hint=(None, None), size=(450, 300))
    popupwindow_err_register_passwords.open()

################################################################################################    
class P_error(FloatLayout):
    pass
def show_popup_error_reg():
    show = P_error()
    global popupwindow_err_register
    popupwindow_err_register = Popup(title="Atention",
                        content=show, size_hint=(None, None), size=(450, 300))
    popupwindow_err_register.open()



#############################################################################################
class P_register(FloatLayout):
    pass
def show_popup_register():
    show = P_register()
    global popupwindow_register
    popupwindow_register = Popup(title="Atention",
                        content=show, size_hint=(None, None), size=(450, 300))
    popupwindow_register.open()
######################################################################################################


class P(FloatLayout):
    pass
def show_popup_logout():
    show = P()
    global popupwindow
    popupwindow = Popup(title="are you shure of logout",
                        content=show, size_hint=(None, None), size=(300, 300))
    popupwindow.open()
    # if openedPop == 0:
    #     popupwindow.open()
    #     openedPop = 1
    # if openedPop == 1:
    #     popupwindow.dismiss()
    #     openedPop = 0


###################################################################################################################


class ScreenManagement(ScreenManager):
    def __init__(self, **kwargs):
        super(ScreenManagement, self).__init__(**kwargs)

    def validate_user(self):
        user = self.ids.username_field
        pwd = self.ids.pwd_field
        info = self.ids.info

        uname = user.text
        passw = pwd.text

        if uname == '' or passw == '':
            info.text = '[color=#FF0000] username and/ or password required [/color]'
        else:
            auth = models.users.AuthgetUser(uname, passw)
            try:
                if auth[1] == True:
                    user_type = auth[0]
                    print('logged In successfully')
                    info.text = ''
                    self.current = "home"
            except TypeError:
                info.text = '[color=#FF0000]Invalid Username and/or password [/color]'

    def btn_popup(self):
        show_popup_logout()

    def btn_ok_popup(self):
        popupwindow.dismiss()
        self.current = "login"
        user = self.ids.username_field
        pwd = self.ids.pwd_field
        user.text = ""
        pwd.text = ""

    def btn_cancel_password_popup(self):
        popupwindow_register.dismiss()
    def btn_ok_createuser_popup(self):
        x = RegScreen
        firstName = x.ids.name_field.text
        LastName = x.ids.ln_field.text
        Age = x.ids.age_field.text
        UserType = "user"
        UserName = x.ids.username_field.text
        Password = x.ids.password_field.text
        Password_repeat = x.ids.pwd_confirm_field.text
        if firstName == "" or LastName == "" or Age == "" or UserName =="" or Password =="":
            popupwindow_register.dismiss()
            show_popup_error_reg()
        else:
            popupwindow_register.dismiss()
            if Password == Password_repeat: 
                result = models.users.newUser(
                firstName, LastName, Age, UserType, UserName, Password)
                print(result)
                firstName = "" 
                LastName = ""
                Age = "" 
                UserName =""
                Password ==""
                self.current = "login"
            else:
                show_popup_error_reg_passwords_not()
    def btn_ok_pass_err(self):
        popupwindow_err_register_passwords.dismiss()

    def btn_cancel_popup(self):
        popupwindow.dismiss()

    def backlogin(self):
        self.current = "login"

    def withoutRegister(self, data):
        print(data)
    def validate_register(self):
        show_popup_register()
    def ok_register_error(self):
         popupwindow_err_register.dismiss()
    
 


class RegisterScreen(Screen): 
    def __init__(self, **kwargs): #init method to run immediately the code is started
        super(RegisterScreen, self).__init__(**kwargs)
        global RegScreen
        RegScreen= self
    pass


class HomeScreen(Screen):
    pass


class CarScreen(Screen):
    pass


class DetailScreen(Screen):
    pass
# LOGIN LAYOUT


class LoginScreen(BoxLayout):
    pass


class LoginApp(App):
    def build(self):
        return ScreenManagement()


if __name__ == '__main__':
    LoginApp().run()


def addProduct():
    Nombre = ""
    tipo = ""
    ruta = ""
    description = ""
    price = ""
    result = models.productos.newProduct(
        user_type, Nombre, tipo, ruta, description, price)
    print(result)


def modifyProduct():
    Nombre = ""
    tipo = ""
    ruta = ""
    description = ""
    price = ""
    id_product = 0
    result = models.productos.modifyProduct(
        user_type, Nombre, Nombre, tipo, ruta, description, price, id_product)
    print(result)


def dropProduct():
    id_product = 0
    result = models.productos.deleteProduct(user_type, id_product)
    print(result)
