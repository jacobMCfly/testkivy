import os.path
import sqlite3
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(BASE_DIR, "database.sqlite")

with sqlite3.connect(db_path) as db:
    cursor = db.cursor()
 
    def newProduct(usertype,Nombre,tipo,ruta,description,price):  
        if (usertype=='master'):
            query = cursor.execute("INSERT INTO Productos VALUES(?,?,?,?,?)",(Nombre,tipo,ruta,description,price)).fetchall()
            if len(query) == 0:
                db.commit()
                return True
            else:
                print("Error al insertar")
    def deleteProduct(usertype,_id):  
        if (usertype=='master'):
            query = cursor.execute("DELETE FROM Productos WHERE id_producto=?",(_id)).fetchall()
            if len(query) == 0:
                db.commit()
                return True
            else:
                print("Ocurrio un error al insertar un producto")
    def modifyProduct(usertype,FirstName,Nombre,tipo,ruta,description,price,_id):  
        if (usertype=='master'):
            query = cursor.execute("UPDATE Productos SET Nombre=?,tipo=?,ruta=?,description=?,price=? WHERE id_producto=? (?,?,?,?,?,?)",(FirstName,Nombre,tipo,ruta,description,price,_id)).fetchall()
            if len(query) == 0:
                db.commit()
                return True
            else:
                print("Ocurrio un error al modificar un producto")
 
    #print(AuthgetUser("mcfly","123456"))