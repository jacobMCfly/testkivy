import kivy
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout

kv = Builder.load_file('inicio.kv')


class InicioApp(App):
    def build(self):
        return kv


if __name__ == '__main__':
    InicioApp().run()
